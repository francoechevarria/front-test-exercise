import { EntityMetadataMap } from '@ngrx/data';

const entityMetadata: EntityMetadataMap = {
  Video: {},
  Musician: {},
  Album: {}
};

const pluralNames = { Video: 'Videos' };

export const entityConfig = {
  entityMetadata,
  pluralNames
};
