export class Video {
  id: number;
  name: string;
  uri: string;
  musician: string;
  album: string;
}
