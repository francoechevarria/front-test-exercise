/**
 * InMemoryDbService with method overrides.
 */
import { Injectable } from '@angular/core';

import {
  RequestInfo,
  RequestInfoUtilities,
  ParsedRequestUrl
} from 'angular-in-memory-web-api';

import {Album, Musician, Video} from './model';

/** In-memory database data */
interface Db {
  [collectionName: string]: any[];
}

@Injectable()
export class InMemoryDataService {
  /** True if in-mem service is intercepting; all requests pass thru when false. */
  active = true;
  maxId = 0;

  /** In-memory database data */
  db: Db = {};

  /** Create the in-memory database on start or by command */
  createDb(reqInfo?: RequestInfo) {
    this.db = getDbData();

    if (reqInfo) {
      const body = reqInfo.utils.getJsonBody(reqInfo.req) || {};
      if (body.clear === true) {
        // tslint:disable-next-line:forin
        for (const coll in this.db) {
          this.db[coll].length = 0;
        }
      }

      this.active = !!body.active;
    }
    return this.db;
  }

  /**
   * Simulate generating new Id on the server
   * All collections in this db have numeric ids.
   * Seed grows by highest id seen in any of the collections.
   */
  genId(collection: { id: number }[], collectionName: string) {
    this.maxId =
      1 +
      collection.reduce((prev, cur) => Math.max(prev, cur.id || 0), this.maxId);
    return this.maxId;
  }

  /**
   * Override `parseRequestUrl`
   * Manipulates the request URL or the parsed result.
   * If in-mem is inactive, clear collectionName so that service passes request thru.
   * If in-mem is active, after parsing with the default parser,
   * @param url from request URL
   * @param utils for manipulating parsed URL
   */
  parseRequestUrl(url: string, utils: RequestInfoUtilities): ParsedRequestUrl {
    const parsed = utils.parseRequestUrl(url);
    const isDefaultRoot = parsed.apiBase === 'api/';
    parsed.collectionName =
      this.active && isDefaultRoot
        ? mapCollectionName(parsed.collectionName)
        : undefined;
    return parsed;
  }
}

/**
 * Remap a known singular collection name ("hero")
 * to the plural collection name ("heroes"); else return the name
 * @param name - collection name from the parsed URL
 */
function mapCollectionName(name: string): string {
  return (
    ({
      video: 'videos',
      musician: 'musicians',
      album: 'albums'
    } as any)[name] || name
  );
}

/**
 * Development data
 */
function getDbData() {
  const videos: Video[] = [
    {
      id: 1,
      uri: 'khttp://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
      name: 'Test video 1',
      musician: 'Rutledge',
      album: 'Album 1'    },
    {
      id: 2,
      uri: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
      name: 'Test video 2',
      musician: 'Janos',
      album: 'Album 2'    },
    {
      id: 4,
      uri: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
      name: 'Test video 3',
      musician: 'Janos.',
      album: 'Album 4'    },
    {
      id: 5,
      uri: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
      name: 'Test video 4',
      musician: 'West Witch',
      album: 'Album 4'
    }
  ];

  const musicians: Musician[] = [
    {
      id: 21,
      name: 'Rutledge'
    },
    {
      id: 22,
      name: 'Janos'
    },
    {
      id: 23,
      name: 'Mira'
    },
    {
      id: 24,
      name: 'Emalee'
    },
    {
      id: 25,
      name: 'West Witch'
    },
    {
      id: 26,
      name: 'Tony Montana'
    }
  ];

  const albums: Album[] = [
    {
      id: 31,
      name: 'Album 1'
    },
    {
      id: 32,
      name: 'Album 2'
    },
    {
      id: 33,
      name: 'Album 3'
    },
    {
      id: 34,
      name: 'Album 4'
    }
  ];

  return { videos, musicians, albums } as Db;
}
