import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Video } from '../../core/model';
import { VideoService } from '../video.service';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {
  loading$: Observable<boolean>;
  selected: Video;
  videos$: Observable<Video[]>;

  constructor(private videoService: VideoService) {
    this.videos$ = videoService.entities$;
    this.loading$ = videoService.loading$;
  }

  ngOnInit() {
    this.getVideos();
  }

  add(video: Video) {
    this.videoService.add(video);
  }

  close() {
    this.selected = null;
  }

  delete(video: Video) {
    this.videoService.delete(video.id);
    this.close();
  }

  enableAddMode() {
    this.selected = <any>{};
  }

  getVideos() {
    this.videoService.getAll();
    this.close();
  }


}
