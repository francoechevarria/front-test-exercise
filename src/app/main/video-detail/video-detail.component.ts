import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges, OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Album, Musician, Video} from '../../core';
import {MusicianService} from '../musician.service';
import {Observable} from 'rxjs';
import {AlbumService} from '../album.service';

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.component.html',
  styleUrls: ['./video-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VideoDetailComponent implements OnChanges, OnInit {
  @Output() unselect = new EventEmitter<string>();
  @Output() add = new EventEmitter<Video>();

  @ViewChild('name', { static: true }) nameElement: ElementRef;

  video: Video;

  form = this.fb.group({
    name: ['', Validators.required],
    uri: ['', Validators.required],
    musician: ['', Validators.required],
    album: ['', Validators.required]
  });

  musicians$: Observable<Musician[]>;
  albums$: Observable<Album[]>;

  constructor(private fb: FormBuilder, private musiciansService: MusicianService, private albumService: AlbumService) {
    this.musicians$ = musiciansService.entities$;
    this.albums$ = albumService.entities$;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setFocus();
    this.form.reset();
  }

  ngOnInit() {
    this.albumService.getAll();
    this.musiciansService.getAll();
  }

  close() {
    this.unselect.emit();
  }

  saveItem(form: FormGroup) {
    if (this.form.valid) {
      this.addItem(form);
    }
  }

  addItem(form: FormGroup) {
    const { value, valid, touched } = form;
    if (touched && valid) {
      this.add.emit({ ...this.video, ...value });
    }
    this.close();
  }

  setFocus() {
    this.nameElement.nativeElement.focus();
  }
}
