import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import {ModalComponent, Video} from '../../core';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VideoListComponent {
  @Input() videos: Video[];
  @Output() deleted = new EventEmitter<Video>();

  constructor(public dialog: MatDialog) {}

  byId(video: Video) {
    return video.id;
  }


  deleteItem(video: Video) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '250px';
    dialogConfig.data = {
      title: 'Delete Album',
      message: `Do you want to delete ${video.name}`
    };

    const dialogRef = this.dialog.open(ModalComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(deleteIt => {
      if (deleteIt) {
        this.deleted.emit(video);
      }
    });
  }
}
