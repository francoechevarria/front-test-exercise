import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../material/material.module';
import { SharedModule } from '../shared/shared.module';
import { VideoDetailComponent } from './video-detail/video-detail.component';
import { VideoListComponent } from './video-list/video-list.component';
import { VideosComponent } from './videos/videos.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: VideosComponent }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    RouterModule.forChild(routes)
  ],
  exports: [VideosComponent, VideoDetailComponent],
  declarations: [VideosComponent, VideoDetailComponent, VideoListComponent]
})
export class MainModule {}
