import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
import { Video } from '../core';

@Injectable({ providedIn: 'root' })
export class VideoService extends EntityCollectionServiceBase<Video> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('Video', factory);
  }
}
