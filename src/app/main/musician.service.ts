import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
import { Musician } from '../core';

@Injectable({ providedIn: 'root' })
export class MusicianService extends EntityCollectionServiceBase<Musician> {
  constructor(factory: EntityCollectionServiceElementsFactory) {
    super('Musician', factory);
  }
}
